# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 22:08:09 2017

@author: Dylan
"""
from floodsystem.stationdata import build_station_list
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.analysis import polyfit
from floodsystem.prediction import predict_water_level_graph
from floodsystem.prediction import predict_highest_level_of_water
from floodsystem.flood import At_Risk_Stations
from floodsystem.datafetcher import fetch_measure_levels
import datetime
def test_call():
    
    station=build_station_list()[0]

    dt = 10
    dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
    
    x1=plot_water_level_with_fit(station, dates, levels, 10)
    x2=polyfit(dates, levels, 10)
    x3=predict_water_level_graph(station,dates,levels,1)
    x4=predict_highest_level_of_water(station,dates,levels,1)
    x5 = At_Risk_Stations(test=False)