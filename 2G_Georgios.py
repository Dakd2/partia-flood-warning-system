# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 21:19:32 2017

@author: Georgios
"""
import datetime
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import update_water_levels
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
from sklearn import svm
from floodsystem.prediction import predict_water_level_graph
from floodsystem.prediction import predict_highest_level_of_water2
from floodsystem.analysis import polyfit

 


def run():
    """Finds the 5 most at risk rivers. 
    Plots the graph of relative water level 1 day ahead and returns the highest relative water value """
    
    stations = build_station_list()
    update_water_levels(stations)
    risky_stations=stations_highest_rel_level(stations,10)
    for station in risky_stations:
        print(station.coord)
    for station in risky_stations:
        print("{} : {}".format(station.name,station.relative_water_level()))
    
    for station in risky_stations:
        try :
            dt = 40
            dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
            predict_water_level_graph(station, dates, levels,1)
            y,x=predict_highest_level_of_water2(station,dates,levels,1)
            x=matplotlib.dates.num2date(x, tz=None)
            print("The maximum level of water will be: {} at {}".format(y,x))
        except KeyError:
            continue

if __name__ == "__main__":

    run()
