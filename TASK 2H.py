# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 01:51:21 2017

@author: Georgios
"""


from floodsystem.datafetcher import fetch_measure_levels
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LassoCV
from sklearn.linear_model import Lasso
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from floodsystem.flood import At_Risk_Stations
import datetime
import warnings
from sklearn import svm
from sklearn import linear_model
from sklearn import metrics

warnings.filterwarnings("ignore")
AtRiskStations = At_Risk_Stations()
dt = 10

for station in AtRiskStations:
    dates, levels = fetch_measure_levels(station[0].measure_id,dt=datetime.timedelta(days=dt))
    dates = matplotlib.dates.date2num(dates)
    dates = dates.reshape(-1,1)
    X = np.array(dates)
    y = np.array(levels)

    
    epsilons = np.logspace(-4, 1, 30)

    scores = []
    scores_std = []
    n_folds = 5

    for epsilon in epsilons:
        clf=svm.SVR(kernel='rbf')
        this_scores = cross_val_score(clf, X, y, cv=n_folds, n_jobs=1)
        scores.append(np.mean(this_scores))
        scores_std.append(np.std(this_scores))

    scores, scores_std = np.array(scores), np.array(scores_std)

    plt.figure().set_size_inches(8, 6)
    plt.semilogx(epsilons, scores)

    # plot error lines showing +/- std. errors of the scores
    std_error = scores_std / np.sqrt(n_folds)

    plt.semilogx(epsilons, scores + std_error, 'b--')
    plt.semilogx(epsilons, scores - std_error, 'b--')

    # alpha=0.2 controls the translucency of the fill color
    plt.fill_between(epsilons, scores + std_error, scores - std_error, epsilon=0.2)
    
    plt.ylabel('CV score +/- std error')
    plt.xlabel('epsilon')
    plt.axhline(np.max(scores), linestyle='--', color='.5')
    plt.xlim([epsilons[0], epsilons[-1]])