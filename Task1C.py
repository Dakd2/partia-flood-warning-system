# -*- coding: utf-8 -*-
"""
Created on Sat Jan 21 15:03:16 2017

@author: Dylan
"""
from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    """ Finds all stations within 10km of central cambridge """
    CamCoord = (52.2053, 0.1218)
    A = stations_within_radius(build_station_list(),CamCoord, 10)
    B = []
    for entry in A:
        B.append(entry.name)
    B.sort()
    print(B)

if __name__ == "__main__":
    run()