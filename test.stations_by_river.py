# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 16:42:18 2017

@author: Georgios
"""

from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
import numpy as np
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

def test_sbr():
    """"Tests that ['Cam', 'Cambridge', 'Cambridge Baits Bite', 
    'Cambridge Jesus Lock', 'Dernford', 'Weston Bampfylde']
    is the list of station by the river Cam"""
    River_Cam = sorted(stations_by_rivers["River Cam"])
    assert River_Cam == ['Cam', 'Cambridge', 'Cambridge Baits Bite', 'Cambridge Jesus Lock', 'Dernford', 'Weston Bampfylde']