# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 17:59:39 2017

@author: Georgios
"""

from floodsystem.flood import At_Risk_Stations
import warnings

warnings.filterwarnings("ignore")
AtRiskStations = At_Risk_Stations()
weight=[]
for r in AtRiskStations:
    y=(100/AtRiskStations[0][1])*r[1]
    weight.append(int(y))
path = 'C:/partia-flood-warning-system/floodrisk.txt'
file = open(path,'w')
for i in range(10):
    file.write("{{location: new google.maps.LatLng{}, weight: {}}},\n".format(AtRiskStations[i][0].coord,weight[i]))
file.close()