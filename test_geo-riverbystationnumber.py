# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 17:46:24 2017

@author: Dylan
"""
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def test_rbsn():
    """Tests river by station number by testing the number of stations on the river tyne, which has 6 stations"""
    A = rivers_by_station_number(build_station_list(),50)
    B = []
    for a in A:
        if a[0] == 'River Tyne':
            B= a
    assert B == ('River Tyne', 6)
