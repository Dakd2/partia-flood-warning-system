# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 21:13:46 2017

@author: Dylan
"""

from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def test_swr():
    """Tests that 11 stations are within 10K of Cambridge as is correct"""
    CamCoord = (52.2053, 0.1218)
    A = stations_within_radius(build_station_list(),CamCoord, 10)
    assert len(A) == 11
