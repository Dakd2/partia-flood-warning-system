# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 11:05:48 2017

@author: Dylan
"""

from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list

def run():
    """Prints names of 10 stations with highest relative water levels and thier relative water levels"""
    RelevantList = stations_highest_rel_level(build_station_list(),10)
    for s in RelevantList:
        print(s.name, s.relative_water_level())

if __name__ == "__main__":
    run()