# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 01:10:41 2017

@author: Georgios
"""

import datetime
import matplotlib
import numpy as np
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.datafetcher import fetch_station_data
import matplotlib.pyplot as plt
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import update_water_levels
from floodsystem.plot import plot_water_levels
import numpy as np
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.analysis import polyfit

def run():
    stations=build_station_list()
    update_water_levels(stations) 
    risky_stations=stations_highest_rel_level(stations,5)
    for station in risky_stations:
        print("{} : {}".format(station.name,station.relative_water_level()))

    for i in range(len(risky_stations)):
        try :
            dt = 10
            dates, levels = fetch_measure_levels(risky_stations[i].measure_id,dt=datetime.timedelta(days=dt))
            plot_water_level_with_fit(risky_stations[i],dates,levels,20)
        except KeyError:
            continue

    dt = 10 
    dates, levels = fetch_measure_levels(risky_stations[0].measure_id,dt=datetime.timedelta(days=dt))
    a=polyfit(dates,levels,10)
    print(a[0])

if __name__ == "__main__":
    run()
