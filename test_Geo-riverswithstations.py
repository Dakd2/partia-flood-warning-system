# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 17:17:27 2017

@author: Dylan
"""
from floodsystem.station import MonitoringStation
from floodsystem.geo import rivers_with_station
from floodsystem.stationdata import build_station_list

def test_rws():
    """tests that rivers with stations works by checking the"""
    StatA = MonitoringStation("test-s-id1", "test-m-id1", "some station1", (-2.0, 4.0), (10, -10), "River X", "Town 1")
    #This station has inconsistent range
    StatB = MonitoringStation("test-s-id2", "test-m-id2", "some station2", (-3.0, 3.0), (-1, None), "River Y", "Town 2")
    StatC = MonitoringStation("test-s-id3", "test-m-id3", "some station3", (-3.0, 3.0), (None, 1), "River Z", "Town 3")
    StatD = MonitoringStation("test-s-id4", "test-m-id4", "some station4", (-3.0, 3.0), (None, None), "River X", "Town 4")
    #These stations have missing range data
    StatE = MonitoringStation("test-s-id5", "test-m-id5", "some station5", (-2.0, 2.0), (-1, 1), "River Y", "Town 5")
    #This station is fine
    
    ExampleStations = [StatA, StatB, StatC, StatD, StatE]
    assert rivers_with_station(ExampleStations) == {'River Y', 'River X', 'River Z'}

    
test_rws()