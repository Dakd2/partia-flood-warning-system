from floodsystem.stationdata import build_station_list


def run():
    """Prints 3 stations and their properties"""

    # Build list of stations
    stations = build_station_list()

    # Count number of stations and print
    print("Number of stations: {}".format(len(stations)))

    # Display data from 3 stations:
    for station in stations:
        if station.name in ['Bourton Dickler', 'Surfleet Sluice', 'Gaw Bridge']:
            print(station)
            print(' ')
    print("Command Complete")


if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")

    # Run Task1A
    run()
