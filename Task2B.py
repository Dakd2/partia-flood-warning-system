# -*- coding: utf-8 -*-
"""
Created on Sat Jan 21 16:49:25 2017

@author: Dylan
"""
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold

def run():
    """Test program which prints a list of every station name and relative level for relative levels over 0.8"""
    stations = build_station_list()
    update_water_levels(stations)
    X = stations_level_over_threshold(stations, 0.8)
    for x in X:
        print(x[0].name, x[1])

if __name__ == "__main__":
    run()