# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 21:05:07 2017

@author: Dylan
"""

from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def test_sbd():
    """Tests that Jesus lock is actually closest station to central cambridge by distance"""
    stations = build_station_list()
    CamCoord = (52.2053, 0.1218)
    distanceList = stations_by_distance(stations, CamCoord)
    ClosestStat = distanceList[0]
    assert ClosestStat[0].name == 'Cambridge Jesus Lock'
    
                      