# -*- coding: utf-8 -*-
"""
Created on Mon Mar 13 17:28:06 2017

@author: Georgios
"""

#Cross validation is computationally expensive. It may take a couple of minutes.

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.svm import SVR
from sklearn.model_selection import GridSearchCV
from floodsystem.flood import At_Risk_Stations
import datetime
import warnings
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.prediction import predict_water_level_graph
def rudfdsfdsn():
    
    warnings.filterwarnings("ignore")
    AtRiskStations = At_Risk_Stations()
    x=0
    for station in AtRiskStations:
        dt=10
        dates, levels = fetch_measure_levels(station[0].measure_id,dt=datetime.timedelta(days=dt))
        dates = matplotlib.dates.date2num(dates)
        dates = dates.reshape(-1,1)
        XTrain = dates
        yTrain = levels
        e_range = 2. ** np.arange(-8, 0, step=2)
        C_range = 2. ** np.arange(-7, 0, step=2)

        parameters = [{'C': C_range, 'epsilon': e_range}]
    
        grid = GridSearchCV(SVR(), parameters, cv=5, n_jobs=4)
        grid.fit(XTrain, yTrain)
    
        bestE = grid.best_params_['epsilon']
        bestC = grid.best_params_['C']
        station.append(bestE)
        station.append(bestC)
        print("station[5] = {}, station[6] = {}".format(station[5],station[6]))
        print ("The best parameters for {} are: epsilon={}  and Cost= {}".format(station[0].name,np.log2(bestE), np.log2(bestC))
    return AtRiskStations
AtRiskStations=rudfdsfdsn()
for station in AtRiskStations:
               print(station[5],station[6])
