# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 21:19:32 2017

@author: Georgios
"""
from floodsystem.plot import polyfit
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import update_water_levels
from floodsystem.stationdata import build_station_list

def predict_maximum_water_level(station,number_of_days):
    dt = 10
    dates, levels = fetch_measure_levels(risky_stations[i].measure_id,dt=datetime.timedelta(days=dt))
    a=polyfit(dates,levels,20)
    
def plot_water_level_with_fit_with_n_days_ahead(station, dates, levels, p,n):
    dates = matplotlib.dates.date2num(dates)
    # Plot original data points
    plt.plot(dates, levels, '.')
    p_coeff = np.polyfit(dates - dates[0], levels, p)
    poly = np.poly1d(p_coeff)
    # Plot polynomial fit at 30 points along interval (note that polynomial
    # is evaluated using the shift x)
    x1 = np.linspace(dates[0], dates[-1]+n, 100)
    plt.plot(x1, poly(x1 - dates[0]))
    # Display plot
    plt.show()

stations=build_station_list()
update_water_levels(stations) 
risky_stations=stations_highest_rel_level(stations,5)
for station in risky_stations:
    print("{} : {}".format(station.name,station.relative_water_level()))

for i in range(len(risky_stations)):
    try :
        dt = 10
        dates, levels = fetch_measure_levels(risky_stations[i].measure_id,dt=datetime.timedelta(days=dt))
        plot_water_level_with_fit_with_n_days_ahead(risky_stations[i], dates, levels,20,1)
    except KeyError:
        continue