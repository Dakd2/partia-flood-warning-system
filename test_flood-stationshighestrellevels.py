# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 17:00:37 2017

@author: Dylan
"""
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list

def test_shrl():
    """Tests stations highest rel level with input 1 does return station with highest rel level"""
    stats = build_station_list()
    A = stations_highest_rel_level(stats, 1)
    print(A)
    
    TotalList = []
    for s in stats:
        if (s.typical_range_consistent() ==True):
            if type(s.relative_water_level()) == float:
                TotalList.append((s,s.relative_water_level()))
    TotalList.sort(key = lambda x: x[1], reverse=True)
    FinalList = [(s[0]) for s in TotalList]
    print(FinalList[0])
    assert [FinalList[0]] == A
    assert len(A)==1
test_shrl()