# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 13:56:02 2017

@author: Dylan
"""

from floodsystem.flood import At_Risk_Stations
import warnings
import matplotlib 
import numpy as np
from floodsystem.prediction import predict_water_level_graph
from floodsystem.datafetcher import fetch_measure_levels
import datetime
def run():
    "Tests At_Risk_Stations in flood"
    warnings.filterwarnings("ignore")
    #gets rid of annoying depreciation warnings from the two points of invalid data that get past the invalid data check function wanted in task 1F
    AtRiskStations = At_Risk_Stations()
    coordinates=[]
    indices=""
    dt = 10
    for station in AtRiskStations:
        print(station[0].town, 'Max Relative level in the next 24 hours: ', str(round(station[1], 3)), 'Risk: ', station[2])
        coordinates.append(list(station[0].coord))
        predict_water_level_graph(station[0],station[3],station[4],1,2**(-8),2**(-2))
        if station[2]=="High":
            indices=indices+"H"
        if station[2]=="Severe":
            indices=indices+"S"
            
    print(coordinates)
    print(indices)
    
        
if __name__ == "__main__":
    run()


