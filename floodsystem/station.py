"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""
from . import datafetcher

class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += " measure id: {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d
    
    def typical_range_consistent(self):
        """This checks if high/low data for that station is consistent, and returns True if it is """
        try:
            Upper = self.typical_range[1]
            Lower = self.typical_range[0]
            if (Upper >= Lower):
                return True
            else:
                return False
        except TypeError:
            return False
        
    def relative_water_level(self):
        """Finds water level as fraction of normal range"""
        if self.typical_range_consistent() == False:
            return None
        try:
            UDiff = self.typical_range[1]- self.typical_range[0]
            CurLev = self.latest_level
            CurrSubtracted = CurLev - self.typical_range[0]
            CurrFrac = CurrSubtracted/ UDiff
            return CurrFrac
        except TypeError:
            return None

def inconsistent_typical_range_stations(stations):
    """Create a list of all the input stations which have inconsistent data"""
    ResultList = []
    for s in stations:
        if s.typical_range_consistent() == False:
            ResultList.append(s)
    return ResultList
