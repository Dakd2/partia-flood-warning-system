# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 21:08:04 2017

@author: Georgios
"""

import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.datafetcher import fetch_station_data
import matplotlib.pyplot as plt
from floodsystem.stationdata import update_water_levels
from floodsystem.plot import plot_water_levels
import numpy as np
import matplotlib

def polyfit(dates, levels, p):
    """Returns the polynomial of degree p that fits the data points of the graph
    produced by plotting levels versus dates."""
    dates = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(dates - dates[0], levels, p)
    poly = np.poly1d(p_coeff)
    x=[]
    x.append(poly)
    x.append(dates[0])
    x=tuple(x)
    return x
    