"""This module contains a collection of functions related to
geographical data.

"""
from .utils import sorted_by_key
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
import numpy as np
from .HAVERSINE import haversine
from operator import itemgetter

def stations_by_distance(stations, p):
    """Takes list of stations and coordinates p, and returns sorted list of (station,distance from p) tuples or (station, town, distance from p) tuples"""
    DistanceList = []
    for stat in stations:
        Coord = stat.coord
        Distance = haversine(Coord,p)
        DistanceList.append((stat, Distance))
    DistanceList.sort(key = lambda x: x[1])    
    return(DistanceList)
        
def rivers_with_station(stations):
    """This function takes a list of stations as an argument and returns a set 
    of all rivers with at least one station."""
    rivers_with_station_set=[]
    for station in stations:
        if station.river == None:
            continue
        else:
            rivers_with_station_set.append(station.river)
    rivers_with_station_set=set(rivers_with_station_set)
    return rivers_with_station_set

def stations_by_river(stations):
    """This function takes as an argument the list of stations and
    returns a Python dict (dictionary) that maps river names 
    (the ‘key’) to a list of stations on a given river.""" 
    rivers={}
    for river in rivers_with_station(stations):
        stations_by_the_river=[]
        for station in stations:
            if station.river != river:
                continue
            else:
                stations_by_the_river.append(station.name)
        rivers[river]=stations_by_the_river
    return rivers

def stations_within_radius(stations, centre, r):
    """Returns list of minotoring stations within r km of point centre"""
    WholeList = stations_by_distance(stations, centre)
    ResultList = []
    for s in WholeList:
        if s[1] <= r:
            ResultList.append(s[0])
    return ResultList



def rivers_by_station_number(stations, N):
    """Returns a list of rivers with the N greatest number of monitoring stations on them"""
    rivers_number_of_stations=[]
    stations_by_given_river=stations_by_river(stations)
    for river in rivers_with_station(stations):
        x=[]
        x.append(river)
        number_of_stations=len(stations_by_given_river[river])
        x.append(number_of_stations)
        x=tuple(x)
        rivers_number_of_stations.append(x)
    
    rivers_number_of_stations=sorted(rivers_number_of_stations,key=itemgetter(1))
    
    m=len(rivers_with_station(stations))    
    i=0
    while rivers_number_of_stations[m-N-i][1]==rivers_number_of_stations[m-N-1-i][1]:
        i+=1
    y=rivers_number_of_stations[m-N-i:]
    y.reverse()
    return y
        

