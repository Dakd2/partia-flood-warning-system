# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 10:40:10 2017

@author: Dylan
"""
from floodsystem.stationdata import update_water_levels
from floodsystem.stationdata import build_station_list
from floodsystem.prediction import predict_highest_level_of_water_tree
from floodsystem.prediction import predict_highest_level_of_water_SVR
from floodsystem.datafetcher import fetch_measure_levels
import datetime


def stations_level_over_threshold(stations, tol):
    """a function that returns a list of tuples, where each tuple holds (1) 
    a station at which the latest relative water level is over tol and (2) the relative water level at the station. 
    The returned list is sorted by the relative level in descending order"""
    update_water_levels(stations)
    ResultList = []
    FailList = []
    newstations = []
    for s in stations:
        if s.typical_range_consistent() ==True:
            newstations.append(s)
    for s in newstations:
        try:
            if s.relative_water_level() > tol:
                ResultList.append((s,s.relative_water_level()))
        except TypeError:
            FailList.append(s)
    ResultList.sort(key = lambda x: x[1], reverse=True)
    return ResultList


def stations_highest_rel_level(stations, N):
    """returns a list of the N stations at which the water level, relative to the typical range, is highest"""
    update_water_levels(stations)
    TotalList = []
    for s in stations:
        if (s.typical_range_consistent() ==True):
            if type(s.relative_water_level()) == float:
                TotalList.append((s,s.relative_water_level()))
    TotalList.sort(key = lambda x: x[1], reverse=True)
    FinalList = [(s[0]) for s in TotalList]
    return FinalList[:N]
# you want s[0] as that pulls the station object out of the tuple it is in

def At_Risk_Stations(test=False):
    """Returns list of 10 most at risk stations as a tuple (station, relative level, risk string)"""
    #Criteria are current level is above 0.7 (as stations below that are no at immenent risk of flooding), and then based on the highest predicted level in 24 hours                                                    
    update_water_levels(build_station_list())
    stats = stations_highest_rel_level(build_station_list(),len(build_station_list()))
    riskstations = []
    if test == True:
        stats = stats[0]
    for s in stats:
        if s.relative_water_level()>0.7:
            
            riskstations.append(s)
    RiskL = []
    SevereRisk= []
    ConsiderableRisk= []
    dt = 10
    for station in riskstations:
        try:
            dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
            UDiff = station.typical_range[1]- station.typical_range[0]
            Maxlev = predict_highest_level_of_water_SVR(station,dates, levels, 1)
            MaxSubtracted = Maxlev - station.typical_range[0]
            rellevel = MaxSubtracted/ UDiff
            if rellevel < 0.5:
                RiskL.append([station,rellevel, 'Low'])
            if rellevel >= 0.5 and rellevel <1 :
                RiskL.append([station,rellevel,'Moderate'])
            if rellevel > 1 and rellevel <= 1.5: 
                RiskL.append([station,rellevel,'High'])
                ConsiderableRisk.append([station,rellevel,'High',dates,levels])
            if rellevel >1.5: 
                RiskL.append([station,rellevel,'Severe'])
                ConsiderableRisk.append([station,rellevel,'Severe',dates,levels])
        except (TypeError, ValueError, KeyError):
            RiskL.append([s, -1000, 'Invalid Information'])
    RiskL.sort(key = lambda x: x[1], reverse = True)
    ConsiderableRisk.sort(key = lambda x: x[1], reverse = True)
   
    return ConsiderableRisk
        