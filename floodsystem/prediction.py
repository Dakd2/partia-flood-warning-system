# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 20:51:28 2017

@author: Georgios
"""

import datetime
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import update_water_levels
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from sklearn import svm
from sklearn import linear_model
from sklearn import tree
from sklearn.neural_network import MLPRegressor

def predict_water_level_graph(station,dates,levels,days,epsilon,cost):
    """Plots the predicted curve days days ahead for the relative level of water versus time.
    This function uses the machine learning algorithm SVR."""
    dates = matplotlib.dates.date2num(dates)
    dates = dates.reshape(-1,1)
    X=list(dates)
    y=list(levels)
    X_train=X[:-days*24*4]
    y_train=y[:-days*24*4]
    #Train SVR
    clf = svm.SVR(C=cost, epsilon=epsilon)
    clf.fit(X_train, y_train) 
    #SVR(C=1.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.1, gamma='auto',kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
    
    
    test_X=X
    #x=dates[0]
    #for i in range(days*24*4):
    #    x=x-1/96
    #   test_X.append(x)
   
    test_y=clf.predict(test_X)
    test_y1=test_y[:-days*24*4]
    test_y2=test_y[-days*24*4:]
    
    plt.plot(X_train,test_y1,'g')
    plt.plot(test_X[-days*24*4:], test_y2,'b')
    plt.plot(X,y,'y')
    plt.title(station.name)
    plt.legend()
    plt.show()
    
def predict_highest_level_of_water_tree(station,dates,levels,days):
    """Predicts the maximum relative level of water days days ahead."""
    dates = matplotlib.dates.date2num(dates)
    dates = dates.reshape(-1,1)
    X=list(dates)
    y=list(levels)
    tree_reg = tree.DecisionTreeRegressor()
    tree_reg = tree_reg.fit(X, y)
    #SVR(C=1.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.1, gamma='auto',kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
    
    test_X=[]
    x=dates[0]
    for i in range(days*24*4):
        x=x+1/96
        test_X.append(x)
        
    test_y=tree_reg.predict(test_X)
    x_y={}
    a=test_y.shape[0]
    for i in range(a):
        x_y[test_y[i]]=test_X[i]
    test_y=list(test_y)
    max_y=max(test_y)
    max_x = x_y[max_y]
    
    return max_y

def predict_highest_level_of_water_SVR(station,dates,levels,days):
    """Predicts the maximum relative level of water days days ahead."""
    dates = matplotlib.dates.date2num(dates)
    dates = dates.reshape(-1,1)
    X=list(dates)
    y=list(levels)
    clf = svm.SVR(C=2**(-3), cache_size=200, coef0=0.0, degree=3, epsilon=2**(-6), gamma='auto',kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
    clf.fit(X, y) 
    #SVR(C=1.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.1, gamma='auto',kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
    
    test_X=[]
    x=dates[0]
    for i in range(days*24*4):
        x=x+1/96
        test_X.append(x)
        
    test_y=clf.predict(test_X)
    x_y={}
    a=test_y.shape[0]
    for i in range(a):
        x_y[test_y[i]]=test_X[i]
    test_y=list(test_y)
    max_y=max(test_y)
    max_x = x_y[max_y]
    
    return max_y

def predict_highest_level_of_water2(station,dates,levels,days):
    """Predicts the maximum relative level of water days days ahead."""
    dates = matplotlib.dates.date2num(dates)
    dates = dates.reshape(-1,1)
    X=list(dates)
    y=list(levels)
    clf = svm.SVR()
    clf.fit(X, y) 
    #SVR(C=1.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.1, gamma='auto',kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)
    
    test_X=[]
    x=dates[0]
    for i in range(days*24*4):
        x=x+1/96
        test_X.append(x)
        
    test_y=clf.predict(test_X)
    x_y={}
    a=test_y.shape[0]
    for i in range(a):
        x_y[test_y[i]]=test_X[i]
    test_y=list(test_y)
    test_X=list(test_X)
    max_y=max(test_y)
    max_x = x_y[max_y]
    max_x=max_x[0]
    return max_y, max_x