# -*- coding: utf-8 -*-
"""
Created on Sun Jan 29 18:31:54 2017

@author: Georgios
"""

import datetime
import matplotlib
import numpy as np
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.datafetcher import fetch_station_data
import matplotlib.pyplot as plt
from floodsystem.stationdata import update_water_levels

def plot_water_levels(station, dates, levels):
    #takes as input the station, the dates and the levels and returns the graph of the relative level versus the dates
    typical_max=[1]*len(dates)
    typical_low=[0]*len(dates)
    
    plt.plot(dates, levels,'r')
    plt.plot(dates,typical_max,'b')
    plt.plot(dates,typical_low,'g')
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()

    
def plot_water_level_with_fit(station, dates, levels, p):
    """takes as input the station, the dates, the levels and the degree p of the polynomial
    which will fit the data points and plots the data points and the polynomial which fits them"""
    dates = matplotlib.dates.date2num(dates)
    # Plot original data points
    plt.plot(dates, levels, '.')
    p_coeff = np.polyfit(dates - dates[0], levels, p)
    poly = np.poly1d(p_coeff)
    # Plot polynomial fit at 30 points along interval (note that polynomial
    # is evaluated using the shift x)
    x1 = np.linspace(dates[0], dates[-1], 30)
    plt.plot(x1, poly(x1 - dates[0]))
    # Display plot
    plt.show()