# -*- coding: utf-8 -*-
"""
Created on Fri Jan 20 10:20:54 2017

@author: Dylan
"""

from floodsystem.HAVERSINE import haversine
def test_haversine():
    """Simple test of Haversine Function"""
    london = (51.5085, -0.1257)
    manchester = (53.4810, -2.2374)
    Thames = (51.500916, -0.123808)
    print(haversine(london, manchester))
    
    assert haversine(london, manchester) == 261.7832740827345  # in kilometers