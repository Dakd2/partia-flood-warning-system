# -*- coding: utf-8 -*-
"""
Created on Sat Jan 21 15:17:50 2017

@author: Dylan
"""

from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list 

def run():
    """tests inconsistent_typical_range"""
    instat = inconsistent_typical_range_stations(build_station_list())
    names = []
    for s in instat:
        names.append(s.name)
    names.sort()
    print(names)


if __name__ == "__main__":
    run()