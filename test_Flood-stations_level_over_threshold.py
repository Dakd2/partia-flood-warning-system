# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 16:25:57 2017

@author: Dylan
"""
from floodsystem.flood import stations_level_over_threshold
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels
from floodsystem.stationdata import build_station_list

def test_slot():
    """Tests that stations level over threshold returns correctly when that station is over the threshold, for first station in list with valid data"""
    n = 0
    Stats = build_station_list()
    for S in Stats:
        if S.typical_range_consistent== True:
            A = Stats[n]
            tol = A.relative_water_level()-0.1
            assert stations_level_over_threshold([A], tol) == (A, A.relative_water_level())
            break