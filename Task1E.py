# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 14:08:01 2017

@author: Dylan
"""

from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    """Returns list of rivers with 9 greatest number of monitoring stations"""
    stations = build_station_list()
    print(rivers_by_station_number(stations, 9))

if __name__ == "__main__":
    run()