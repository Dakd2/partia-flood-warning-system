# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 15:34:49 2017

@author: Dylan
"""
from floodsystem.station import MonitoringStation
def rwl():
     StatA = MonitoringStation("test-s-id1", "test-m-id1", "some station1", (0.5, 0.9), (1.0, 2.0), "River X", "Town 1")
     StatB = MonitoringStation("test-s-id2", "test-m-id2", "some station2", (2.0, 3.0), (1.0, 2.0), "River Y", "Town 2")
     StatA.latest_level = 2.0
     StatB.latest_level = 1.0
     A = StatA.relative_water_level()
     B = StatB.relative_water_level()
     return([A,B])
assert rwl()[0]== 1.0
assert rwl()[1]== 0.0
