# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
import numpy as np
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

def run():
    stations=build_station_list()
    print('Number of Rivers with Monitoring station(s): ', len(rivers_with_station(stations)))
    rivers_with_stations=list(rivers_with_station(stations))
    rivers_with_stations=sorted(rivers_with_stations)
    print('First 10 river names in alphabetical order:')
    for i in range(10):
        print (rivers_with_stations[i])

    stations_by_rivers=stations_by_river(stations)
    River_Aire = sorted(stations_by_rivers["River Aire"])
    print('Names of all stations on River Aire are:')
    print(River_Aire)

    River_Cam = sorted(stations_by_rivers["River Cam"])
    print('Names of all stations on River Cam are:')
    print(River_Cam)

    Thames = sorted(stations_by_rivers["Thames"])
    print('Names of all stations on River Thames are:')
    print(Thames)

if __name__ == "__main__":
    run()