# -*- coding: utf-8 -*-
"""
Created on Fri Jan 20 15:31:46 2017

@author: Dylan
"""
from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list
StationList = build_station_list()

def run():
    """Tests stations_by_distance by inputting coords for london and the actual station list"""
    stations = build_station_list()
    CamLoc= (52.2053, 0.1218)
    stations_by_dist = stations_by_distance(stations, CamLoc)
    nearest_stations = []
    for station, d in stations_by_dist:
        nearest_stations.append((station.name, station.town, d))
    print("Closest 10 Stations are: ")
    print(nearest_stations[:10])
    print("Furthest 10 Stations are: ")
    print(nearest_stations[-10:])

if __name__ == "__main__":
    run()

